A code to create a large number of Stochastic Source Time Functions (SSTF) to be used for ground motion simulation using and empirical Green’s function approach.

This code constrains the mean value and variability of the stress drop parameter using the Source Time Functions duration obtained from a global data base SCARDEC (Vallée and Douet, 2016; Courboulex et al, 2016). The SSTFs are then computed by a stochastic function following the Kohrs-Sansorny et al. (2005) method.

=========================================
Requeriments
=========================================
To run the code are necesary the python 3.x libreries:


- numpy
- matplotlib
- tqdm

=========================================
Inputs
=========================================

The basic inputs of the code must be defined inside a Input file in a text format. By default the txt file name is: "Input_STF.csv"

Inside the input file the parameters are defined in the second column of each line. The column delimiter is by default ",".
The parameters must be defined by lines in the next order:


1. Corner frequency of the event to be scaled. **The input should be a float number**.
2. Seismic moment of the target event. **The input should be a float number**.
3. Seismic moment of the EGF event. T**he input should be a float number**.
4. Number of STF will be created. **The input must be an integer (int)**.
5. The time step of the created STF. dt has to be the same than dt the records have. **The input is a float**.
6. Folder that will be created and the plots will be store. **It must be an string**
7. q parameter to relate duration with the corner frequency (Frequency_corner=q/duration). **The input is a float**.
8. Definition of the kind of source. The method manage two brands, Subduction(1) - Not Subduction(0).** Must be an Int**.
9. Number of plots that will be create. **It must be an Int**
10. Folder that will be create for store the created STF. **The input is an string.**
11. Number from the code starts to name the created STF. **It is an int**

=========================================
Running
=========================================

Once defined the inputs, the code must be run with python 3. There are two brands of the code with similar results but with a considerable difference in the time of computation.

1. STF.py: This codes follows the Kohrs-Sansorny et al., 2005 method strictly to create the STF.
2. STF_HP.py: This codes follows the  Kohrs-Sansorny et all, 2005 method approximating the rho function by an interpolation, instead of solving the equation each time. This results in a considerable improvement in the time of computation, but creates a small rise of energy at high frequencies in comparison with the expected one by the theory.

=========================================
References
=========================================
1. Kohrs-Sansorny, C., F. Courboulex, M. Bour, and A. Deschamps, 2005, A Two-Stage Method for Ground-Motion Simulation Using Stochastic Summation of Small Earthquakes, 4, Bulletin of the Seismological Society of America, 95, no. 4, 1387–1400, doi: 10.1785/0120040211.
2. Courboulex, F., M. Vallée, M. Causse, and A. Chounet, 2016, Stress‐Drop Variability of Shallow Earthquakes Extracted from a Global Database of Source Time Functions, Seismological Research Letters, 87, no. 4, 912–918, doi: 10.1785/0220150283.
3. Vallée, M., and V. Douet, 2016, A new database of source time functions (STFs) extracted from the SCARDEC method, Physics of the Earth and Planetary Interiors, 257, 149–157, doi: 10.1016/j.pepi.2016.05.012.

