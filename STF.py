# -*- coding: utf-8 -*-
"""
@author: David Alejandro Castro Cruz
da.castro790@uniandes.edu.co
"""
import numpy as np
import matplotlib.pyplot as plt
from os import path, makedirs
from tqdm import tqdm
def calf(Fnum,Fden):
    '''

    :param Fnum:
    :param Fden:
    :return: alpha
    '''

    return 2.0 / (1 + (Fden / Fnum) ** 2)

def rho(tb,Fnum,Fden, alpha):
    '''
    :param tb: Must not be zero, the integral doesn't converge
    :param Fnum: Frequency in the numerator
    :param Fden: Frequency un the denominator
    :param alpha:
    :return: Rho
    '''
    t = np.fabs(tb)
    if t>1/Fden:
        #  Super machetazo para grandes (y muy inprobables) valores.
        #  Para grandes valores la integración numérica no es sufucientemente exacta (razón por la cual esto es hecho)
        n1 = 3
        nd = 10000
        n2 = nd / n1
        t=1/Fden
        lsup = n1 / t
        df = 1 / (t * n2)
        nd = nd + 1
        fra = np.linspace(0, lsup, nd, endpoint=True)
        fev = np.sqrt(1 + alpha * (fra / Fnum) ** 2) * np.cos((2 * np.pi * t) * fra) / (
        1 + (fra / Fden) ** 2)  # symetric
        vv = np.arange(0, nd - 1, 4, dtype=int)

        rhodb = np.pi * Fden * np.exp(-2 * np.pi * Fden * t) #Fden y t se cancelan !!!!!!!!!!!
        fcc=4 * df * np.sum(7 * (fev[vv] + fev[vv + 4]) + 32 * (fev[vv + 1] + fev[vv + 3]) + fev[vv + 2]) / (45*rhodb)

        return fcc*(0.99*t/np.fabs(tb)+0.01) * np.pi * Fden * np.exp(-2 * np.pi * Fden * np.fabs(tb))
        # (t/(100*np.fabs(tb))+0.01) is a "help" to try to follow the continuation of the function)
    else:
        nd=int(np.ceil(100/t))
        if nd<10000:
            nd=10000
            n1=3
        elif nd<10000000:
            n1 = 2
            while nd%4!=0:  # The method to solve the integral requires a number multiply of 4
                nd+=1
        else:
            nd=10000000
            n1 = 1
            if t<1E-7:
                t=1E-7  # Machetazo, el método solo fue provado para valores superiores a 1E-6
    lsup = n1 / t

    n2 = nd / n1
    df = 1/(t*n2)
    nd=nd+1
    fra=np.linspace(0,lsup, nd, endpoint=True)
    fev=np.sqrt(1 + alpha * (fra / Fnum) ** 2) * np.cos((2 * np.pi * t) * fra) / (1 + (fra / Fden) ** 2)  # symetric
    vv=np.arange(0,nd-1,4,dtype=int)

    return 4 * df * np.sum(7*(fev[vv]+fev[vv+4])+32*(fev[vv+1]+fev[vv+3])+fev[vv+2]) / 45

def rhoMasa(tb,Fnum,Fden, alpha):
    '''

    :param tb: Must be: 1E-7>tb AND tb < T(=1/Fden)
    :param Fnum: Frequency in the numerator
    :param Fden: Frequency un the denominator
    :param alpha:
    :return: Rho for an array of ts
    '''
    t = np.fabs(tb)
    n1=np.empty(t.size, dtype=np.int)
    nd=np.asarray(np.ceil(100 / t), dtype=np.int)

    nd[nd<10000]=10000
    n1[nd<10000]=3

    # The method to solve the integral requires a number multiply of 4
    c1 = np.all([[nd < 10000000], [nd >= 10000], [nd % 4 == 0]], axis=0)[0]
    nd[c1] = nd[c1]
    n1[c1] = 2
    c1 = np.all([[nd < 10000000],[nd >= 10000],[nd%4==1]], axis=0)[0]
    nd[c1] = nd[c1]+3
    n1[c1] = 2
    c1 = np.all([[nd < 10000000], [nd > 10000], [nd % 4 == 2]], axis=0)[0]
    nd[c1] = nd[c1] + 2
    n1[c1] = 2
    c1 = np.all([[nd < 10000000], [nd > 10000], [nd % 4 == 3]], axis=0)[0]
    nd[c1] = nd[c1] + 1
    n1[c1] = 2

    nd[nd >= 10000000] = 10000000
    n1[nd >= 10000000] = 1

    lsup = n1 / t

    n2 = nd / n1
    df = 1 / (t * n2)
    nd = nd+ 1
    res=np.empty(nd.size)
    for n in range(nd.size):
        fra = np.linspace(0, lsup[n], nd[n], endpoint=True)
        fev = np.sqrt(1 + alpha * (fra / Fnum) ** 2) * np.cos((2 * np.pi * t[n]) * fra) / (1 + (fra / Fden) ** 2)
        # symetric
        vv = np.arange(0, nd[n] - 1, 4, dtype=np.int)
        res[n]=4 * df[n] * np.sum(7 * (fev[vv] + fev[vv + 4]) + 32 * (fev[vv + 1] + fev[vv + 3]) + fev[vv + 2]) / 45
    return res
def tgen(Fnum,Fden,c, alf):
    '''
    
    :param Fnum: Frequency in the numerator
    :param Fden: Frequency un the denominator
    :param c: Param for refuse
    :param alf: alpha
    :return: random time, around zero with rho density of probability
    '''
    ld = 1.0
    rhof=0.0
    while ld>rhof:
        u=np.random.rand(2)
        if u[0] < 0.5:
            t = 0.5*np.log(2*u[0])/(np.pi*Fden)
        else:
            t = -0.5*np.log(2*(1-u[0]))/(np.pi*Fden)
        ld=u[1]*c*np.pi*Fden*np.exp(-2*np.pi*Fden*np.fabs(t))

        rhof = rho(t, Fnum, Fden,alf)

    return t

def tgenFast(Fden, c, rho_L):
    '''

    :param Fnum: Frequency in the numerator
    :param Fden: Frequency un the denominator
    :param c: Param for refuse
    :param alf: alpha
    :param rho_L: defined[:1/Fden] value to interpolate in log scale rho_c
    :return: random time, around zero with rho density of probability
    '''
    ld = 1.0
    rhof = 0.0
    while ld > rhof:
        u = np.random.rand(2)
        if u[0] < 0.5:
            t = 0.5 * np.log(2 * u[0]) / (np.pi * Fden)
        else:
            t = -0.5 * np.log(2 * (1 - u[0])) / (np.pi * Fden)
        ld = u[1] * c * np.pi * Fden * np.exp(-2 * np.pi * Fden * np.fabs(t))

        tb=np.fabs(t)
        rhof = np.interp(tb, rho_L[0], rho_L[1], left=-1,
                         right=rho_L[1][-1]* np.exp(-2 * np.pi*Fden*tb)/(np.exp(-2 * np.pi))*(0.99/(Fden*t)+0.01))
    return t

def nextpow2(n):
    m_f = np.log2(n)
    m_i = np.ceil(m_f)
    return int(2 ** m_i)
def fourSpec(data, frem, tnd=-1):
    if tnd==-1: tnd=nextpow2(data.size)
    FT = np.abs(np.fft.rfft(data, tnd))
    df = frem / tnd
    return [np.arange(df, 0.5 * frem, df), FT/frem]
#  Kohrs-Sansorny et all 2005
#  Courboulex et all 2016
#Afile=input(['Name of input file: (Input_STF.csv)'])
#if Afile=='':
Afile='Input_STF.csv'
try:
    fls = open(Afile, 'r')
    fc = float(fls.readline().split(',')[1])
    Mo = float(fls.readline().split(',')[1])
    mo = float(fls.readline().split(',')[1])
    nsim = int(fls.readline().split(',')[1])
    dt = float(fls.readline().split(',')[1])
    folp = fls.readline().split(',')[1][:-1]
    q = float(fls.readline().split(',')[1])
    case = int(fls.readline().split(',')[1])
    ncop = int(fls.readline().split(',')[1])
    outf = fls.readline().split(',')[1][:-1]
    innu = int(fls.readline().split(',')[1])

    if folp[-1]!='/': folp = folp+'/'
    if not path.exists(folp): makedirs(folp[:-1])
    if outf[-1]!='/': outf = outf+'/'
    if not path.exists(outf): makedirs(outf[:-1])
    if ncop > 0:
        fols = folp + 'Temporal_STF/'
        if not path.exists(fols):
            makedirs(fols[:-1])
        fig2 = plt.figure()
except FileNotFoundError:
    print('The input file was not found')
    exit()
except:
    fls.close()
    print('The input file was not understood, please sort data and put it after "," symbol eg:')
    print('fc[Hz],#f\nMw[Nm],#f\nmw[Nm],#f\nnsim,#i\ndelta_t[s],%f\nFoldPlots,%s\nq[1],%f\n'
          'case sub(1)/nosub(0),%i\nnplots,#i')
    exit()
fls.close()
if case==1:
    para = (0.2867054, -4.32051131)
    dsl = 0.3254309677058605  # ln(Standard deviation)
elif case==0:
    para = (0.31011871, -4.90319434)
    dsl = 0.3475617334577107  # ln(Standard deviation)
else:
    print('Model case was not understood. Either 1, for subduction, or 0, for not-subduction')
    exit()

Tc = 10 ** (para[0] * np.log10(Mo) + para[1])
miu = np.log(Tc / np.exp(0.5 * dsl ** 2))

frem = 1 / dt
Tce = 6 * Tc
lrm = 12 * Tc * frem
lrs = int(round(lrm))
lrm = lrs * dt
FS = np.zeros(int(np.round(0.5 * nextpow2(lrs))) - 1)

crcr = np.empty((nsim, 2))
crdr = np.empty((nsim, 2))

lss=np.exp(miu+2*dsl)
lis=np.exp(miu-2*dsl)
fig1=plt.figure()

Fc=np.empty(nsim)
for ns in tqdm(range(innu, nsim)):
    #print('%d de %d' % (ns, nsim))
    tcc=np.random.lognormal(miu, dsl)
    while tcc < lis or tcc > lss:  # cutting the distribution
        tcc = np.random.lognormal(miu, dsl)
    Fc[ns] = q/tcc  # Relation rupture duration and corner frequency

    N = fc / Fc[ns]
    eta = N ** 4
    kappa = Mo / (mo * eta) / dt
    etac = int(N ** 2)
    etad = int(round(eta / etac))

    Fd = (etac ** 0.25) * Fc[ns]
    alc = calf(Fd, Fc[ns])
    ald = calf(fc, Fd)
    crc = rho(1E-7, Fd, Fc[ns], alc) / (np.pi * Fc[ns] * np.exp(-2 * np.pi * Fc[ns]*1E-7))
    crd = rho(1E-7, fc, Fd, alc) / (np.pi * Fd * np.exp(-2 * np.pi * Fd*1E-7))

    crcr[ns] = Fc[ns], crc
    crdr[ns] = Fd, crd
    # STF computation
    RS = np.zeros(lrs + 1)
    vtc = np.concatenate((10 ** np.arange(-6, np.log10(dt) - 0.1, 1.0), np.arange(dt, Tc + 0.2 * dt, 1.5*dt)))
    rhoc = rhoMasa(vtc, Fd, Fc[ns], alc)
    vtd = np.concatenate((10 ** np.arange(-6, np.log10(dt) - 0.1, 1.0), np.arange(dt, 1 / Fd + 0.2 * dt, 1.5*dt)))
    rhod = rhoMasa(vtd, fc, Fd, ald)
    for nc in range(etac):
        #tc = tgen(Fd, Fc[ns], crc, alc)
        tc = tgenFast(Fc[ns], crc, (vtc, rhoc))
        for nd in range(etad):
            #tfi = tc + tgen(fc, Fd, crd, ald) + Tce
            tfi = tc + tgenFast(Fd, crd, (vtd, rhod)) + Tce
            if tfi < 0:
                Tce = Tce - tfi
                RS = np.append(np.zeros(int(np.round(-tfi * frem))), RS)
                RS[0] += kappa
            elif tfi > lrm:
                RS = np.append(RS, np.zeros(int(np.round((tfi - lrm) * frem))))
                lrm = tfi
                RS[-1] += kappa
            else:
                RS[int(np.round(tfi * frem))] += kappa
    FT = fourSpec(RS, frem)
    FS += FT[1][1:-1]
    plt.figure(fig1.number)
    plt.plot(FT[0], FT[1][1:-1], color=(0.6, 0.6, 0.6))

    #  Record STF
    rsp = np.where(RS != 0)
    STF = RS[rsp[0][0]:(rsp[0][-1] + 1)]

    #  Writing process
    fsa=open('%sSTF%d.txt'%(outf,ns),'w')
    fsa.write('Fc:\t%f\tMo:\t%f\nfc:\t%f\tmo:\t%f\ndt[s]:\t%f\nq:\t%f\n' % (Fc[ns], Mo, fc, mo, dt, q))
    for amS in STF:
        fsa.write('%f\n'%amS)
    fsa.close()
    if ns<ncop:
        plt.figure(fig2.number)
        plt.clf()
        plt.plot(np.arange(0,STF.size)*dt, STF, color=(0,0.2,0.8))
        plt.ylabel('Amplitude', fontsize=14)
        plt.xlabel('Time [s]', fontsize=14)
        plt.savefig('%sSTF_%d.png' % (fols,ns+1))
plt.figure(fig1.number)
Fcg = q / Tc
plt.plot(FT[0], FS / nsim, color=(0, 0.2, 0.8))
plt.plot(FT[0], (Mo / mo) * (1 + (FT[0] / fc) ** 2) / (1 + (FT[0] / Fcg) ** 2), color=(0.9, 0.5, 0))
plt.axvline(Fcg, color=(0, 0, 0))
plt.axvline(fc, color=(0, 0, 0))
plt.xscale('log')
plt.yscale('log')
plt.ylabel('FAS(STF)', fontsize=14)
plt.xlabel('Frequency [Hz]', fontsize=14)

plt.savefig('%sRFou_STFA.png' % folp)
if nsim>50:
    plt.clf()
    plt.hist(q/Fc, 50)
    plt.grid()
    plt.xlabel('Fault duration [s]', fontsize=14)
    plt.savefig('%sHistogramDuration.jpeg' % folp,dpi=300)


print('¡¡¡Final!!!')
